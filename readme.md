Kudos to Dave Gandy who has created an incredible cool icon font called [Font Awesome][1].

This font fits perfectly into JavaFX as

* all these beautiful icons are scalable vector graphics
* each icon (unicode character) can be styled with css
* incredible lightweight (one font 519 icons)

This project provides a helper class (AwesomeDude) for easy usage of these icons in JavaFX apps.


#Currently FontAwesome 4.3.0 is supported.


#Usage
* [8.0.10 announcement][5]
* [8.0.9 announcement][6]
* [8.0.7/8.0.8 announcement][7]
* [blog post2][3]
* [blog post1][2]

#Maven
    <dependency>
        <groupId>de.jensd</groupId>
        <artifactId>fontawesomefx</artifactId>
        <version>8.0.13</version>
    </dependency>

#License
FontAwesomeFX is licensed under the [Apache 2.0 license][4].
If this license is not suitable, please contact me to discuss an alternative license.

[1]: http://fortawesome.github.com/Font-Awesome/
[2]: http://www.jensd.de/wordpress/?p=692
[3]: http://www.jensd.de/wordpress/?p=733
[4]: http://www.apache.org/licenses/LICENSE-2.0.html
[5]: http://www.jensd.de/wordpress/?p=1556
[6]: http://www.jensd.de/wordpress/?p=1457
[7]: http://www.jensd.de/wordpress/?p=1182